import * as d3 from 'd3';

export async function chorotest(elementId: string, geojsonPath: string, options: any, width = 975, height = 610) {
	const geoData: any = await d3.json(geojsonPath);

  if (options.propertyFilter) {
    delete(geoData.bbox)
    geoData.features = geoData.features.filter(el=> el?.properties?.[options.propertyFilter.key] === options.propertyFilter.value)
  }

  console.log(geoData)

	// Append an SVG element to the map container
	const svg = d3.select(elementId).append('svg').attr('width', width).attr('height', height);

	// Define the color scale based on your property values
	const colorScale = d3
		.scaleSequential()
		.domain(options?.domain ? options.domain : [0, 10]) // Customize the domain based on your data
		.interpolator(d3.interpolateReds); // Customize the color scheme

	// Create a projection for the map
	const projection = d3.geoMercator().fitSize([width, height], geoData);

	// Create a path generator
	const path = d3.geoPath().projection(projection);

	// Append paths for each feature in the GeoJSON data
	svg
		.selectAll('path')
		.data(geoData.features)
		.enter()
		.append('path')
		.attr('d', path)
		.attr('fill', (d: any) => colorScale(d.properties?.[options?.valueKey])); // Replace 'value' with the name of your property
}
